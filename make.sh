#!/bin/zsh
THEME=default2
TITLE=kyle.log
OUTPUT=site
FORMAT=markdown
UPLOAD_REMOTE=log.kyleisom.net:site
UPLOAD_DIR=$HOME/sites/log/
UPLOAD_HOST=port.tyrfingr.is

do_build () {
        chronicle --blog-title="${TITLE}"			\
		  --theme=${THEME} --theme-dir="themes/"	\
		  --output=${OUTPUT} --input=blog/
}

do_upload () {
	# If the site hasn't been built yet, build it.
	if [ ! -d "$OUTPUT" ]
	then
		do_build
	fi

	# Double check that it's been built.
	if [ ! -d "$OUTPUT" ]
	then
		echo "[!] unable to build site"
		exit 1
	fi

	# When building on the www site, don't do a remote copy.
	if [[ "$(hostname)" == "$UPLOAD_HOST" ]]
	then
		UPLOAD=$UPLOAD_DIR
	else
		UPLOAD=$UPLOAD_REMOTE
	fi

	rsync -auvz $OUTPUT/ $UPLOAD/
}

do_setup () {
	sudo cp upstream/chronicle.list /etc/apt/sources.list.d
	sudo apt-key add upstream/apt-key.pub
	sudo apt-get install apt-transport-https
	sudo apt-get remove chronicle
	sudo apt-get update
	sudo apt-get install libchronicle-perl libtext-markdown-perl libdbd-sqlite3-perl
}

normalise () {
	echo "$1" | tr '[A-Z]' '[a-z]' |	\
		sed -e 's/ /_/g' 		\
		    -e 's/[,.!?]//g' 		\
		    -e 's/:/-/g'
}

build_new () {
	pdate="$(date +%Y-%m-%d)"
	slug=$(normalise $1)
	path="blog/${pdate}_${slug}.txt"
	/bin/cat > $path <<EOF
title: $1
date: $pdate
tags:
format: markdown

EOF
	[ -z "$EDITOR" ] && EDITOR=/usr/bin/mg
	$EDITOR $path
}

case $1 in
	upload)
		do_upload
		;;
	clean)
		[ -d "$OUTPUT" ] && rm -rf $OUTPUT
		;;
	realclean)
		[ -d "$OUTPUT" ] && rm -rf $OUTPUT
		[ -e blog.db ] && rm -f blog.db
		;;
	setup)
		do_setup
		;;
	new)
		if [ -z "$2" ]
		then
			echo "[!] no post title provided"
			exit 1
		fi
		build_new $2
		;;
	*)
		do_build
		;;
esac	
