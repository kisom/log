THEME :=	default2
CONFIG :=	config
TITLE :=	kyle.log
OUTPUT :=	site
FORMAT :=	markdown
UPLOAD_DIR :=	log.kyleisom.net:site

.PHONY: all
all:
	chronicle --blog-title="$(TITLE)"			\
		  --theme=$(THEME) --theme-dir="themes/"	\
		  --output=${OUTPUT} --input=blog
upload:
	rsync --delete-after -auvz $(OUTPUT)/ $(UPLOAD_DIR)/

setup:
	sudo cp upstream/chronicle.list /etc/apt/sources.list.d
	sudo apt-key add upstream/apt-key.pub
	sudo apt-get install apt-transport-https
	sudo apt-get remove chronicle
	sudo apt-get update
	sudo apt-get install chronicle libtext-markdown-perl

.PHONY: clean
clean:
	rm -rf $(OUTPUT)

.PHONY: realclean
realclean: clean
	rm -f blog.db
